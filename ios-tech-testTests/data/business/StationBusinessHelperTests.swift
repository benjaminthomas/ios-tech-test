//
//  StationBusinessHelper.swift
//  ios-tech-testTests
//
//  Created by Benjamin THOMAS on 18/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import XCTest
@testable import ios_tech_test
@testable import Swinject

class StationBusinessHelperTests: XCTestCase {
    private let dataContainer: Container = Container()
    private let testAreaID: String = "stop_area:0:SA:8768600"
    
    private var persistenceManager: PersistenceManager!
    
    // MARK: Actual values
    private var actualValueFavorite: Bool?
    private var actuelValuePlaceEntitiesWithFav: [PlaceEntity] = []
    
    override func setUpWithError() throws {
        dataContainer.register(PersistenceManager.self) { (resolver: Resolver) -> PersistenceManager in
            PersistenceManagerImpl()
        }.inObjectScope(.container)
        
        UserDefaults.standard.removeObject(forKey: testAreaID)
    }
    
    override class func tearDown() {
        UserDefaults.standard.removeObject(forKey: "stop_area:0:SA:8768600")
    }
    
    func testAddFavorite() throws {
        givenPersistenceManagerContainingNoFavorites()
        whenAddingFavorites()
        thenExpectFavoritesAdded()
    }
    
    func testRemoveFavorite() throws {
        givenPersistenceManagerContainingNoFavorites()
        whenRemovingFavorites()
        thenExpectFavoritesRemoved()
    }
    
    func testAddFavoriteToEntityes() {
        let placeEntitiesMock: [PlaceEntity] = generatePlaceEntitiesMock()
        givenPersistenceManagerContainingNoFavorites()
        
        whenCheckingIfPlaceIsFavorite(mock: placeEntitiesMock)
        thenExpectEntitiesWithFavorite()
    }
}

private extension StationBusinessHelperTests {
    // MARK: Given
    
    func givenPersistenceManagerContainingNoFavorites() {
        persistenceManager = dataContainer.resolve(PersistenceManager.self)!
    }
    
    // MARK: When
    
    func whenAddingFavorites() {
        persistenceManager.addFavorite(id: testAreaID)
        actualValueFavorite = persistenceManager.isFavorite(id: testAreaID)
    }
    
    func whenRemovingFavorites() {
        persistenceManager.removeFavorite(id: testAreaID)
        actualValueFavorite = persistenceManager.isFavorite(id: testAreaID)
    }
    
    func whenCheckingIfPlaceIsFavorite(mock: [PlaceEntity]) {
        persistenceManager.addFavorite(id: testAreaID)
        actuelValuePlaceEntitiesWithFav = mock.map {[weak self] placeEntity in
            guard let self = self else { return placeEntity }
            var placeEntityFav = placeEntity
            placeEntityFav.isFavorite = self.persistenceManager.isFavorite(id: placeEntity.id)
            return placeEntityFav
        }
    }
    
    // MARK: Then
    
    func thenExpectFavoritesAdded() {
        XCTAssertEqual(actualValueFavorite, true)
    }
    
    func thenExpectFavoritesRemoved() {
        XCTAssertEqual(actualValueFavorite, false)
    }
    
    func thenExpectEntitiesWithFavorite() {
        let entityWithFavorite = actuelValuePlaceEntitiesWithFav[0]
        let entityWithOutFavorite = actuelValuePlaceEntitiesWithFav[1]
        let entity2WithOutFavorite = actuelValuePlaceEntitiesWithFav[2]
        
        XCTAssertEqual(entityWithFavorite.isFavorite, true)
        XCTAssertEqual(entityWithOutFavorite.isFavorite, false)
        XCTAssertEqual(entity2WithOutFavorite.isFavorite, false)
    }
    
    // MARK: Mock
    func generatePlaceEntitiesMock() -> [PlaceEntity] {
        [
            PlaceEntity(id: testAreaID, name: "nameTest"),
            PlaceEntity(id: "0001", name: "nameTest"),
            PlaceEntity(id: "0002", name: "nameTest"),
        ]
    }
}
