//
//  TransportMapperTests.swift
//  ios-tech-testTests
//
//  Created by Benjamin THOMAS on 18/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import XCTest
@testable import ios_tech_test
@testable import Swinject

class TransportMapperTests: XCTestCase {
    private let mapperContainer: Container = Container()
    private var transportMapper: TransportMapper!
    
    var actualValue: Departure.Transport?
    
    override func setUpWithError() throws {
        mapperContainer.register(TransportMapper.self) { _ -> TransportMapper in
            TransportMapper()
        }.inObjectScope(.container)
    }
    
    func testTranformStringToBus() {
        givenTransportMapper()
        whenTransportMapperTranform(entry: "bus")
        thenExpectTypeBus()
    }
    
    func testTranformStringToTram() {
        givenTransportMapper()
        whenTransportMapperTranform(entry: "train")
        thenExpectTypeTramway()
    }
    
    func testTranformStringToMetro() {
        givenTransportMapper()
        whenTransportMapperTranform(entry: "métro")
        thenExpectTypeMetro()
    }
}

private extension TransportMapperTests {
    func givenTransportMapper() {
        self.transportMapper = mapperContainer.resolve(TransportMapper.self)!
    }
    
    func whenTransportMapperTranform(entry: String) {
        actualValue = transportMapper.transform(entry)
    }
    
    func thenExpectTypeBus() {
        XCTAssertEqual(actualValue, Departure.Transport.bus)
    }
    
    func thenExpectTypeMetro() {
        XCTAssertEqual(actualValue, Departure.Transport.metro)
    }
    
    func thenExpectTypeTramway() {
        XCTAssertEqual(actualValue, Departure.Transport.tramway)
    }
}
