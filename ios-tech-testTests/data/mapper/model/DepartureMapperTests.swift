//
//  DepartureMapperTests.swift
//  ios-tech-testTests
//
//  Created by Benjamin THOMAS on 18/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import XCTest
@testable import ios_tech_test
@testable import Swinject

class DepartureMapperTests: XCTestCase {
    private let mapperContainer: Container = Container()
    private var departureMapper: DepartureMapper!
    
    var actualValue: [Departure]?
    
    override func setUpWithError() throws {
        mapperContainer.register(TransportMapper.self) { _ -> TransportMapper in
            TransportMapper()
        }.inObjectScope(.container)
        
        mapperContainer.register(DepartureMapper.self) { (resolver: Resolver) -> DepartureMapper in
            DepartureMapper(transportMapper: resolver.resolve(TransportMapper.self)!)
        }.inObjectScope(.container)
    }
    
    func testDepartureMapperTransformEntities() {
        let departureEntities: [DepartureEntity] = departurEntitiesMock()
        givenDepartureMapper()
        whenDepartureMapperTransformEntities(entry: departureEntities)
        thenExpectDeparture()
    }
    
    func testDepartureMapperTransformValideDate() {
        let departureEntities: [DepartureEntity] = departurEntitiesMock()
        givenDepartureMapper()
        whenDepartureMapperTransformEntities(entry: departureEntities)
        thenExpectValideDate()
    }
}

private extension DepartureMapperTests {
    func givenDepartureMapper() {
        departureMapper = mapperContainer.resolve(DepartureMapper.self)!
    }
    
    func whenDepartureMapperTransformEntities(entry: [DepartureEntity]) {
        actualValue = departureMapper.transform(entry)
    }
    
    func thenExpectDeparture() {
        XCTAssertEqual(actualValue?.count, 2)
    }
    
    func thenExpectValideDate() {
        guard let expectedDate = actualValue?[0].departureAt else { XCTFail(); return }
        XCTAssertTrue(expectedDate is Date)
    }
    
    func departurEntitiesMock() -> [DepartureEntity] {
        [
            DepartureEntity(name: "depart1", label: "1", transport: "métro", color: "#ffff", departureAt: "20200818T113100"),
            DepartureEntity(name: "depart2", label: "2", transport: "bus", color: "#ffff", departureAt: "20200818T113100"),
            DepartureEntity(name: "departfailDate", label: "3", transport: "bus", color: "#ffff", departureAt: "aa")
        ]
    }
}
