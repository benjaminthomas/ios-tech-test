//
//  ViewControllerProvider.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit
import Swinject

final class ViewControllerProvider {
    private let sharedContainer: Container
    private let mapperAssembly: MapperAssembly
    private let dataAssembly: DataAssembly
    private let presentationAssembly: PresentationAssembly
    
    init(
        dataAssembly: DataAssembly,
        mapperAssembly: MapperAssembly,
        presentationAssembly: PresentationAssembly
    ) {
        self.mapperAssembly = mapperAssembly
        self.dataAssembly = dataAssembly
        self.presentationAssembly = presentationAssembly

        sharedContainer = Container()
        self.dataAssembly.assemble(container: sharedContainer)
        self.mapperAssembly.assemble(container: sharedContainer)
        self.presentationAssembly.assemble(container: sharedContainer)
    }
}

extension ViewControllerProvider {
    func navigation() -> UINavigationController {
        let navigationController = UINavigationController()
        navigationController.navigationBar.prefersLargeTitles = true
        return navigationController
    }
    
    func homeViewController(router: Router) -> Presentable {
        let assembler = Assembler([HomeViewAssembly()], container: sharedContainer)
        return assembler.resolver.forceResolve(HomeViewController.self, arguments: router, self)
    }
    
    func departuresViewController(router: Router, place: Place) -> Presentable {
        let assembler = Assembler([DeparturesAssembly()], container: sharedContainer)
        return assembler.resolver.forceResolve(DeparturesViewController.self, arguments: router, self, place)
    }
}
