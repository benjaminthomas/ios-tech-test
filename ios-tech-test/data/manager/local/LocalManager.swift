//
//  LocalManager.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 18/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Foundation

class LocalManagerImpl: PersistenceManager {
    private let userDefaults: UserDefaults = UserDefaults.standard
    
    func addFavorite(id: String) {
        userDefaults.set(true, forKey: id)
    }
    
    func removeFavorite(id: String) {
        userDefaults.removeObject(forKey: id)
    }
    
    func isFavorite(id: String) -> Bool {
        userDefaults.bool(forKey: id)
    }
}
