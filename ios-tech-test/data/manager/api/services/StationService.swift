//
//  StationService.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Moya

private enum StationServiceValues {
    static let placesPath = "/places/"
    static let placeDeparturesPath = "/stop_areas/"
}

struct StationService: TargetType, AccessTokenAuthorizable {
    var baseURL: URL {
        return URL(string: ApiValues.BASE_URL)!
    }
    
    var path: String
    var method: Moya.Method
    var sampleData: Data
    var task: Task
    var headers: [String: String]?
    var validationType: ValidationType
    var authorizationType: AuthorizationType?
    
    init(
        path: String = "",
        method: Moya.Method = .get,
        task: Task = .requestPlain,
        validationType: ValidationType = .successAndRedirectCodes,
        sampleJsonString: String = "",
        headers: [String: String]? = nil,
        authorizationType: AuthorizationType = .basic
    ) {
        self.path = path
        self.method = method
        self.sampleData = Data(sampleJsonString.utf8)
        self.task = task
        self.validationType = validationType
        self.headers = headers
        self.authorizationType = authorizationType
    }
}

extension StationService {
    /// Get places
    /// - HTTP Method: GET
    /// - Response PlacesResponseRemoteEntity
    static func getPlaces(query: String) -> StationService {
        let parameters: [String: Any] = ["q": query]
        
        
        return StationService(
            path: StationServiceValues.placesPath,
            method: .get,
            task: .requestParameters(parameters: parameters, encoding: URLEncoding.default),
            authorizationType: .basic
        )
    }
    
    /// Get place departures
    /// - HTTP Method: GET
    /// - Response PlacesResponseRemoteEntity
    static func getPlaceDepartues(for area: String) -> StationService {
        let path: String = StationServiceValues.placeDeparturesPath + "\(area)/departures"
        
        return StationService(
            path: path,
            method: .get,
            task: .requestParameters(parameters: [:], encoding: URLEncoding.queryString),
            authorizationType: .basic
        )
    }
}

