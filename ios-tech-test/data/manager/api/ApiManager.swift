//
//  ApiManager.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

protocol ApiManager {
    func retrievePlace(by: String, completionHandler: @escaping (_ result: PlacesResponseRemoteEntity?, _ error: Error?) -> Void)
    func retriveDepartures(stopArea: String, completionHandler: @escaping (DeparturesResponseRemoteEntity?, Error?) -> Void)
}
