//
//  ApiManagerImpl.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Moya

class ApiManagerImpl: ApiManager {
    
    private var stationProvider: MoyaProvider<StationService>
    
    init() {
        let credentials: CredentialsPlugin = CredentialsPlugin { target -> URLCredential? in
            URLCredential(
                user: ApiValues.API_KEY,
                password: "",
                persistence: .none
            )
        }
        
        self.stationProvider = MoyaProvider<StationService>(plugins: [credentials])
    }
    
    func retrievePlace(
        by query: String,
        completionHandler: @escaping (PlacesResponseRemoteEntity?, Error?) -> Void
    ) {
        stationProvider.request(StationService.getPlaces(query: query)) { result in
            do {
                let response = try result.get()
                let value = try response.mapJSON()
                let data = try JSONSerialization.data(withJSONObject: value as Any, options: [])
                let entityDecoded = try JSONDecoder().decode(PlacesResponseRemoteEntity.self, from: data)
                completionHandler(entityDecoded, nil)
            } catch {
                completionHandler(nil, error)
            }
        }
    }
    
    func retriveDepartures(
        stopArea: String,
        completionHandler: @escaping (DeparturesResponseRemoteEntity?, Error?) -> Void
    ) {
        stationProvider.request(StationService.getPlaceDepartues(for: stopArea)) { result in
            do {
                let response = try result.get()
                let value = try response.mapJSON()
                let data = try JSONSerialization.data(withJSONObject: value as Any, options: [])
                let entityDecoded = try JSONDecoder().decode(DeparturesResponseRemoteEntity.self, from: data)
                completionHandler(entityDecoded, nil)
            } catch {
                completionHandler(nil, error)
            }
        }
    }
}
