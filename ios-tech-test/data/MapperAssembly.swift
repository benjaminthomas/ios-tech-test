//
//  MapperAssembly.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Swinject

class MapperAssembly: Assembly {
    func assemble(container: Container) {
        assembleEntityMappers(in: container)
        assembleModelMappers(in: container)
    }
}

private extension MapperAssembly {
    func assembleEntityMappers(in container: Container) {
        container.register(PlaceEntityMapper.self) { _ -> PlaceEntityMapper in
            PlaceEntityMapper()
        }
        .inObjectScope(.container)
    }
    
    func assembleModelMappers(in container: Container) {
        container.register(PlaceMapper.self) { _ -> PlaceMapper in
            PlaceMapper()
        }
        .inObjectScope(.container)
        
        container.register(TransportMapper.self) { _ -> TransportMapper in
            TransportMapper()
        }
        .inObjectScope(.container)
        
        container.register(DepartureMapper.self) { (resolver: Resolver) -> DepartureMapper in
            DepartureMapper(transportMapper: resolver.forceResolve(TransportMapper.self))
        }
        .inObjectScope(.container)
    }
}
