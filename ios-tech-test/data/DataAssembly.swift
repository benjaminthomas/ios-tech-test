//
//  DataAssembly.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Swinject

class DataAssembly: Assembly {
    func assemble(container: Container) {
        assembleManagers(in: container)
        assembleBusinessHelpers(in: container)
        assembleRepositories(in: container)
    }
}

private extension DataAssembly {
    func assembleManagers(in container: Container) {
        container.register(ApiManager.self) { _ -> ApiManager in
            ApiManagerImpl()
        }
        .inObjectScope(.container)
        
        container.register(DepartureEntityMapper.self) { _ -> DepartureEntityMapper in
            DepartureEntityMapper()
        }
        .inObjectScope(.container)
        
        container.register(PersistenceManager.self) { _ -> PersistenceManager in
            PersistenceManagerImpl()
        }
        .inObjectScope(.container)
    }
    
    func assembleBusinessHelpers(in container: Container) {
        container.register(StationBusinessHelper.self) { (resolver: Resolver) -> StationBusinessHelper in
            StationBusinessHelper(
                apiManager: resolver.forceResolve(ApiManager.self),
                persistenceManager: resolver.forceResolve(PersistenceManager.self),
                placeEntityMapper: resolver.forceResolve(PlaceEntityMapper.self),
                departureEntityMapper: resolver.forceResolve(DepartureEntityMapper.self))
        }
        .inObjectScope(.container)
    }
    
    func assembleRepositories(in container: Container) {
        container.register(ContentRepository.self) { (resolver: Resolver) -> ContentRepository in
            ContentRepository(
                stationBusinessHelper: resolver.forceResolve(StationBusinessHelper.self),
                placeMapper: resolver.forceResolve(PlaceMapper.self),
                departureMapper: resolver.forceResolve(DepartureMapper.self)
            )
        }
        .inObjectScope(.container)
    }
}
