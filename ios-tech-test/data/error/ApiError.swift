//
//  ApiError.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 18/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

enum ApiError: Error {
    case resultNil
}
