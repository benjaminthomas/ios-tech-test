//
//  StopDateTimeRemoteEntity.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

struct StopDateTimeRemoteEntity: Decodable {
    let departureDate: String
    
    private enum CodingKeys : String, CodingKey {
        case departureDate = "base_departure_date_time"
    }
}
