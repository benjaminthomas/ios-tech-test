//
//  DeparturesResponseRemoteEntity.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

struct DeparturesResponseRemoteEntity: Decodable {
    let departures: [DepartureRemoteEntity]
}
