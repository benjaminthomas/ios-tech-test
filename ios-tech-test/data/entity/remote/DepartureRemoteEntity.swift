//
//  DepartureRemoteEntity.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

struct DepartureRemoteEntity: Decodable {
    let display_informations: DepartureDisplayInformationRemoteEntity
    let stop_date_time: StopDateTimeRemoteEntity
}
