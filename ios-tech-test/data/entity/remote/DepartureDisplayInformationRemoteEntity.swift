//
//  DepartureDisplayInformationRemoteEntity.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

struct DepartureDisplayInformationRemoteEntity: Decodable {
    let direction: String
    let label: String
    let commercial_mode: String
    let color: String
    let trip_short_name: String
}
