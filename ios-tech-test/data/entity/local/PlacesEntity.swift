//
//  PlacesEntity.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

struct PlaceEntity {
    let id: String?
    let name: String?
}
