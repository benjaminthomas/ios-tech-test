//
//  DepartureEntity.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

struct DepartureEntity {
    let name: String
    let label: String
    let transport: String
    let color: String
    let departureAt: String
}
