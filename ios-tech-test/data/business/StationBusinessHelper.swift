//
//  StationBusinessHelper.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

class StationBusinessHelper {
    private let apiManager: ApiManager
    private let persistenceManager: PersistenceManager
    
    private let placeEntityMapper: PlaceEntityMapper
    private let departureEntityMapper: DepartureEntityMapper
    
    init(
        apiManager: ApiManager,
        persistenceManager: PersistenceManager,
        placeEntityMapper: PlaceEntityMapper,
        departureEntityMapper: DepartureEntityMapper
    ) {
        self.apiManager = apiManager
        self.persistenceManager = persistenceManager
        self.placeEntityMapper = placeEntityMapper
        self.departureEntityMapper = departureEntityMapper
    }
    
    func retrievePlaces(
        query: String,
        completionHandler: @escaping ([PlaceEntity]?, Error?) -> Void
    ) {
        apiManager.retrievePlace(by: query, completionHandler: {[weak self] result, error in
            guard let self = self else { return }
            
            if let error = error {
                completionHandler(nil, error)
                return
            }
            
            guard let placesResponseRemoteEntity = result else {
                completionHandler(nil, ApiError.resultNil)
                return
            }
            
            let placeEntities: [PlaceEntity] = self.placeEntityMapper.transform(placesResponseRemoteEntity.places)
            let placeEntitiesWithFav: [PlaceEntity] = placeEntities.map {[weak self] placeEntity in
                guard let self = self else { return placeEntity }
                var placeEntityFav = placeEntity
                placeEntityFav.isFavorite = self.persistenceManager.isFavorite(id: placeEntity.id)
                return placeEntityFav
            }
            
            completionHandler(placeEntitiesWithFav, nil)
        })
    }
    
    func retriveDepartures(
        area: String,
        completionHandler: @escaping ([DepartureEntity]?, Error?) -> Void
    ) {
        apiManager.retriveDepartures(
            stopArea: area,
            completionHandler: {[weak self] result, error in
                guard let self = self else { return }
                
                if let error = error {
                    completionHandler(nil, error)
                    return
                }
                
                guard let departuresResponseRemoteEntity = result?.departures else {
                    completionHandler(nil, ApiError.resultNil)
                    return
                }
                
                let departureEntities: [DepartureEntity] = self.departureEntityMapper.transform(departuresResponseRemoteEntity)
                completionHandler(departureEntities, nil)
        })
    }
    
    func addOrRemoveFavorite(with area: String) {
        if persistenceManager.isFavorite(id: area) {
            persistenceManager.removeFavorite(id: area)
        } else {
            persistenceManager.addFavorite(id: area)
        }
    }
}
