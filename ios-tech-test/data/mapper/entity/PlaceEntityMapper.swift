//
//  PlacesEntityMapper.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

class PlaceEntityMapper {
    func transform(_ remote: PlaceRemoteEntity) -> PlaceEntity? {
        guard let id = remote.id else { return nil }
        return PlaceEntity(
            id: id,
            name: remote.name
        )
    }

    func transform(_ remotes: [PlaceRemoteEntity]) -> [PlaceEntity] {
        remotes.compactMap(transform(_:))
    }
}
