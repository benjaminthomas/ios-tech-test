//
//  DepartureEntityMapper.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

class DepartureEntityMapper {
    func transform(_ remote: DepartureRemoteEntity) -> DepartureEntity {
        DepartureEntity(
            name: remote.display_informations.trip_short_name,
            label: remote.display_informations.label,
            transport: remote.display_informations.commercial_mode,
            color: remote.display_informations.color,
            departureAt: remote.stop_date_time.departureDate
        )
    }

    func transform(_ remotes: [DepartureRemoteEntity]) -> [DepartureEntity] {
        remotes.map(transform(_:))
    }
}
