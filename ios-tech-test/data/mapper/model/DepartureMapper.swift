//
//  DepartureMapper.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Foundation

class DepartureMapper {
    private let transportMapper: TransportMapper
    private let dateFormat: String = "yyyyMMdd'T'HHmmss"
    private let dateFormatter: DateFormatter
    
    init(transportMapper: TransportMapper) {
        self.transportMapper = transportMapper
        
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
    }
    
    func transform(_ entity: DepartureEntity) -> Departure? {
        let isoDate = entity.departureAt
        guard let date = dateFormatter.date(from: isoDate) else { return nil }
        
        return Departure(
            name: entity.name,
            label: entity.label,
            departureAt: date,
            color: entity.color,
            transport: transportMapper.transform(entity.transport)
        )
    }

    func transform(_ remotes: [DepartureEntity]) -> [Departure] {
        remotes.compactMap(transform(_:))
    }
}
