//
//  PlaceMapper.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

class PlaceMapper {
    func transform(_ entity: PlaceEntity) -> Place {
        return Place(
            id: entity.id,
            name: entity.name,
            isFavorite: entity.isFavorite
        )
    }

    func transform(_ remotes: [PlaceEntity]) -> [Place] {
        remotes.map(transform(_:))
    }
}
