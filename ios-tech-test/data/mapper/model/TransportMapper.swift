//
//  TransportMapper.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

class TransportMapper {
    func transform(_ entity: String) -> Departure.Transport {
        switch entity.uppercased() {
        case "MÉTRO":
            return .metro
        case "BUS":
            return .bus
        case "TRAIN":
            return .tramway
        default:
            return .unknow
        }
    }
}
