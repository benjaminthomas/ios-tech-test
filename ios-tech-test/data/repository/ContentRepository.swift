//
//  ContentRepository.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

class ContentRepository {
    private let stationBusinessHelper: StationBusinessHelper
    
    private let placeMapper: PlaceMapper
    private let departureMapper: DepartureMapper
    
    init(
        stationBusinessHelper: StationBusinessHelper,
        placeMapper: PlaceMapper,
        departureMapper: DepartureMapper
    ) {
        self.stationBusinessHelper = stationBusinessHelper
        self.placeMapper = placeMapper
        self.departureMapper = departureMapper
    }
    
    func retrievePlaces(
        query: String,
        completionHandler: @escaping ([Place]?, Error?) -> Void
    ) {
        stationBusinessHelper
            .retrievePlaces(query: query) {[weak self] (result, error) in
                guard let self = self else { return }

                if let error = error {
                    completionHandler(nil, error)
                    return
                }
                
                guard let placeEntities = result else {
                    completionHandler(nil, ApiError.resultNil)
                    return
                }
                
                let places: [Place] = self.placeMapper.transform(placeEntities)
                completionHandler(places, nil)
        }
    }
    
    func retrieveDepartures(
        from selectedArea: String,
        completionHandler: @escaping ([Departure]?, Error?) -> Void
    ) {
        stationBusinessHelper
            .retriveDepartures(area: selectedArea) {[weak self] (result, error) in
                guard let self = self else { return }
                if let error = error {
                    completionHandler(nil, error)
                    return
                }
                
                guard let departureEntities = result else {
                    completionHandler(nil, ApiError.resultNil)
                    return
                }
                
                let departures: [Departure] = self.departureMapper.transform(departureEntities)
                completionHandler(departures, nil)
        }
    }
    
    func favoriteAction(with aera: String) {
        stationBusinessHelper.addOrRemoveFavorite(with: aera)
    }
}
