//
//  Departure.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Foundation

struct Departure {
    enum Transport {
        case metro
        case tramway
        case bus
        case unknow
    }
    
    let name: String
    let label: String
    let departureAt: Date
    let color: String
    let transport: Transport
}
