//
//  AppNavigator.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

class AppNavigator: Navigator {
    enum Destination {
        case home
    }

    private weak var viewControllerProvider: ViewControllerProvider?
    private weak var router: Router?

    init(viewControllerProvider: ViewControllerProvider, router: Router) {
        self.viewControllerProvider = viewControllerProvider
        self.router = router
    }

    func navigate(to destination: Destination) {
        guard let viewControllerProvider = viewControllerProvider,
              let router = router else {
            preconditionFailure()
        }
        
        switch destination {
        case .home:
            router.setRootModule(viewControllerProvider.homeViewController(router: router))
        }
    }
}
