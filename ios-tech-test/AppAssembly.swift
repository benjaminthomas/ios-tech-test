//
//  AppAssembly.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Swinject

class AppAssembly: Assembly {
    func assemble(container: Container) {
        container.register(AppNavigator.self) { (_: Resolver, viewControllerProvider: ViewControllerProvider, router: Router) -> AppNavigator in
            AppNavigator(
                viewControllerProvider: viewControllerProvider,
                router: router
            )
        }
    }
}
