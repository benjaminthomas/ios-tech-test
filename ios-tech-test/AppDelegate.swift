//
//  AppDelegate.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private var appAssembler: Assembler!
    private var appAssembly: AppAssembly!
    private var dataAssembly: DataAssembly!
    private var mapperAssembly: MapperAssembly!
    private var presentationAssembly: PresentationAssembly!
    
    private var viewControllerProvider: ViewControllerProvider!
    private var applicationRouter: Router!
    private var rootNavigationController: UINavigationController!
    
    private var appNavigator: AppNavigator!
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        prepareDependencyInjection()
        prepareApp()

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootNavigationController
        window?.makeKeyAndVisible()
        
        appNavigator.navigate(to: .home)
        
        setupAppearance()
        
        return true
    }
}

private extension AppDelegate {
    func prepareDependencyInjection() {
        appAssembly = AppAssembly()
        dataAssembly = DataAssembly()
        mapperAssembly = MapperAssembly()
        presentationAssembly = PresentationAssembly()
    }
    
    func prepareApp() {
        let viewControllerProvider = ViewControllerProvider(dataAssembly: dataAssembly, mapperAssembly: mapperAssembly, presentationAssembly: presentationAssembly)
        
        self.rootNavigationController = viewControllerProvider.navigation()
        let router: Router = RouterImpl(rootController: rootNavigationController)
        
        self.viewControllerProvider = viewControllerProvider
        self.applicationRouter = router
        
        let appAssembler = Assembler([dataAssembly, appAssembly])
        self.appNavigator = appAssembler.resolver.resolve(AppNavigator.self, arguments: viewControllerProvider, router)!
    }
    
    func setupAppearance() {
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.backgroundColor = R.color.main()!
            appearance.titleTextAttributes = [
                .foregroundColor: UIColor.white,
                .font: R.font.poppinsRegular(size: 21)!
            ]
            appearance.largeTitleTextAttributes = [
                .foregroundColor: UIColor.white,
                .font: R.font.poppinsBold(size: 36)!
            ]

            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            UINavigationBar.appearance().tintColor = .white
            UINavigationBar.appearance().barTintColor = R.color.main()!
            UINavigationBar.appearance().isTranslucent = false
        }
    }
}
