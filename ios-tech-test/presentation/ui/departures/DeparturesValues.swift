//
//  DeparturesValues.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 18/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import CoreGraphics

enum DeparturesValues {
    static let CELL_CORNER_RADIUS: CGFloat = 20.0
    static let CELL_SHADOW_OPACITY: Float = 0.2
    static let CELL_SHADOW_RADIUS: CGFloat = 10.0
    
    static let TABLEVIEW_CONTENT_INSET_TOP: CGFloat = 24.0
}
