//
//  DeparturesViewController.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit

class DeparturesViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet private weak var departuresTablesView: UITableView!
    
    // MARK: - Properties
    private var navigator: DeparturesNavigator!
    private var viewModel: DeparturesViewModel!
    
    private var departures: [DepartureViewDataWrapper] = []

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
        retriveData()
    }
}

private extension DeparturesViewController {
    func setupUI() {
        title = viewModel.placeTitle
        
        departuresTablesView.delegate = self
        departuresTablesView.dataSource = self
        departuresTablesView.register(
            UINib(nibName: R.nib.departureCell.name, bundle: nil),
            forCellReuseIdentifier: R.reuseIdentifier.departureCell.identifier
        )
        departuresTablesView.contentInset = .init(top: DeparturesValues.TABLEVIEW_CONTENT_INSET_TOP, left: 0, bottom: 0, right: 0)
    }
    
    func bindViewModel() {
        viewModel.departures = {[weak self] departures in
            guard let self = self else { return }
            self.departures = departures
            self.departuresTablesView.reloadData()
        }
    }
    
    func retriveData() {
        viewModel.retrieveDepartures()
    }
}

extension DeparturesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        departures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.departureCell.identifier, for: indexPath) as? DepartureCell else { return UITableViewCell () }
        let departureViewDataWrapper = departures[indexPath.row]
        cell.setupCell(with: departureViewDataWrapper)
        return cell
    }
}

// MARK: - MakeViewController
extension DeparturesViewController {
    static func makeViewController(navigator: DeparturesNavigator, viewModel: DeparturesViewModel) -> DeparturesViewController {
        guard let viewController = R.storyboard.departures.departuresViewController() else {
            preconditionFailure()
        }
        
        viewController.navigator = navigator
        viewController.viewModel = viewModel

        return viewController
    }
}
