//
//  DeparturesAssembly.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Swinject

class DeparturesAssembly: Assembly {
    func assemble(container: Container) {
        container.register(DeparturesNavigator.self) { (_: Resolver, viewControllerProvider: ViewControllerProvider, router: Router) -> DeparturesNavigator in
            DeparturesNavigator()
        }
        
        container.register(DeparturesViewModel.self) { (resolver: Resolver, place: Place) -> DeparturesViewModel in
            DeparturesViewModel(
                place: place,
                contentRepository: resolver.forceResolve(ContentRepository.self)
            )
        }
        
        container.register(DeparturesViewController.self) { (resolver: Resolver, router: Router, viewControllerProvider: ViewControllerProvider, place: Place) -> DeparturesViewController in
            DeparturesViewController.makeViewController(
                navigator: resolver.forceResolve(DeparturesNavigator.self, arguments: viewControllerProvider, router),
                viewModel: resolver.forceResolve(DeparturesViewModel.self, argument: place)
            )
        }
    }
}
