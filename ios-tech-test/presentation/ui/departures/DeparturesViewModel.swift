//
//  DeparturesViewModel.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit

class DeparturesViewModel {
    private let place: Place
    private let contentRepository: ContentRepository
    
    var placeTitle: String {
        place.name ?? ""
    }
    
    var departures: ([DepartureViewDataWrapper]) -> Void = { _ in }
    var departuresError: (Error) -> Void = { _ in }
    
    init(place: Place, contentRepository: ContentRepository) {
        self.place = place
        self.contentRepository = contentRepository
    }
    
    func retrieveDepartures() {
        contentRepository.retrieveDepartures(from: place.id) {[weak self] departures, error in
            guard let self = self else { return }
            if let error = error {
                self.departuresError(error)
                return
            }
            
            guard let departuresWrapped = departures else { return }
            self.departures(departuresWrapped.map { DepartureViewDataWrapper($0) })
        }
    }
}
