//
//  DepartureCell.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit

class DepartureCell: UITableViewCell {
    @IBOutlet private weak var mainView: UIView!
    @IBOutlet private weak var transportImageView: UIImageView!
    @IBOutlet private weak var codeView: UIView!
    @IBOutlet private weak var codeLabel: UILabel!
    @IBOutlet private weak var startDateLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var nameLabelWidthConstraint: NSLayoutConstraint!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        codeView.layer.cornerRadius = codeView.frame.height / 2
        mainView.layer.cornerRadius = DeparturesValues.CELL_CORNER_RADIUS
        
        mainView.layer.shadowColor = UIColor.lightGray.cgColor
        mainView.layer.shadowOffset = .zero
        mainView.layer.shadowOpacity = DeparturesValues.CELL_SHADOW_OPACITY
        mainView.layer.shadowRadius = DeparturesValues.CELL_SHADOW_RADIUS
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    func setupCell(with departure: DepartureViewDataWrapper) {
        transportImageView.image = departure.image
        codeView.backgroundColor = departure.color
        codeLabel.text = departure.code
        
        startDateLabel.text = departure.departureDate
        nameLabel.text = departure.name
        
        nameLabelWidthConstraint.constant = departure.departureWidth
    }
}

private extension DepartureCell {
    func commonInit() {
        selectionStyle = .none
        backgroundColor = .clear
    }
}
