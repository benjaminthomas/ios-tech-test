//
//  HomeViewModel.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

class HomeViewModel {
    private let contentRepository: ContentRepository
    
    var places: ([PlaceViewDataWrapper]) -> Void = { _ in }
    var placesError: (Error) -> Void = { _ in }
    
    init(contentRepository: ContentRepository) {
        self.contentRepository = contentRepository
    }
    
    func retrievePlace(query input: String) {
        contentRepository
            .retrievePlaces(query: input) {[weak self] (places, error) in
                guard let self = self else { return }
                if let error = error {
                    self.placesError(error)
                    return
                }
                
                guard let placesWrapped = places else { return }
                self.places(placesWrapped.map { PlaceViewDataWrapper($0) })
        }
    }
    
    func favoriteAction(with aera: String) {
        contentRepository.favoriteAction(with: aera)
    }
}
