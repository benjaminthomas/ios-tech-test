//
//  HomeViewAssembly.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Swinject

class HomeViewAssembly: Assembly {
    func assemble(container: Container) {
        container.register(HomeNavigator.self) { (resolver: Resolver, router: Router, viewControllerProvider: ViewControllerProvider) -> HomeNavigator in
            HomeNavigator(
                viewControllerProvider: viewControllerProvider,
                router: router
            )
        }
        
        container.register(HomeViewModel.self) { (resolver: Resolver) -> HomeViewModel in
            HomeViewModel(contentRepository: resolver.forceResolve(ContentRepository.self))
        }
        
        container.register(HomeViewController.self) { (resolver: Resolver, router: Router, viewControllerProvider: ViewControllerProvider) -> HomeViewController in
            HomeViewController.makeViewController(
                navigator: resolver.forceResolve(HomeNavigator.self, arguments: router, viewControllerProvider),
                viewModel: resolver.forceResolve(HomeViewModel.self)
            )
        }
    }
}
