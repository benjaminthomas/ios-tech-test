//
//  PlaceCell.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {
    @IBOutlet private weak var favoriteButton: UIButton!
    @IBOutlet private weak var placeLabel: UILabel!
    
    private var placeViewDataWrapper: PlaceViewDataWrapper?
    
    var favoriteTap: ((_ id: String) -> Void)?
    
    func setup(with placeViewDataWrapper: PlaceViewDataWrapper) {
        self.placeViewDataWrapper = placeViewDataWrapper
        selectionStyle = .none
        placeLabel.text = placeViewDataWrapper.name
        setupFavoriteImage(isFavorite: placeViewDataWrapper.isFavorite)
    }
    
    @IBAction func favoriteAction(_ sender: Any) {
        guard let placeViewDataWrapper = placeViewDataWrapper else { return }
        favoriteTap?("\(placeViewDataWrapper.aera)")
        setupFavoriteImage(isFavorite: !placeViewDataWrapper.isFavorite)
    }
}

private extension PlaceCell {
    func setupFavoriteImage(isFavorite: Bool) {
        favoriteButton.setImage(
            isFavorite ? R.image.star_full()! : R.image.start_empty()!,
            for: .normal
        )
    }
}
