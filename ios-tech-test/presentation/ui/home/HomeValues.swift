//
//  HomeValues.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import CoreGraphics

enum HomeValues {
    static let HEIGHT_CELL: CGFloat = 50.0
}
