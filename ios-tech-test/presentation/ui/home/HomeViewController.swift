//
//  ViewController.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet private weak var placesTableview: UITableView!
    @IBOutlet private weak var errorLabel: UILabel!
    
    private var searchController: UISearchController!
    private var places: [PlaceViewDataWrapper] = []
    
    private var viewModel: HomeViewModel!
    private var navigator: HomeNavigator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewModel()
    }
}

private extension HomeViewController {
    func setupUI() {
        title = R.string.localized.homeTitle()
        
        searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = R.string.localized.homeSearchbarPlaceholder()
        searchController.searchBar.tintColor = .white
        searchController.searchBar.compatibleSearchTextField.backgroundColor = .white
        searchController.hidesNavigationBarDuringPresentation = true
        
        navigationItem.searchController = searchController
        definesPresentationContext = false
        
        placesTableview.delegate = self
        placesTableview.dataSource = self
        placesTableview.register(
            UINib(nibName: R.nib.placeCell.name, bundle: nil),
            forCellReuseIdentifier: R.reuseIdentifier.placeCell.identifier
        )
        
        errorLabel.text = R.string.localized.appErrorNetwork()
        errorLabel.isHidden = true
    }
    
    func bindViewModel() {
        viewModel.places = {[weak self] places in
            guard let self = self else { return }
            self.errorLabel.isHidden = true
            self.places = places
            self.placesTableview.reloadData()
        }
        
        viewModel.placesError = {[weak self] error in
            guard let self = self else { return }
            self.errorLabel.isHidden = false
            self.places.removeAll()
            self.placesTableview.reloadData()
        }
    }
}

extension HomeViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let string = searchController.searchBar.text,
            string.count > 1 else { return }
        viewModel.retrievePlace(query: string)
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.placeCell.identifier, for: indexPath) as? PlaceCell else { return UITableViewCell() }
        cell.setup(with: places[indexPath.item])
        cell.favoriteTap = {[weak self] area in
            self?.viewModel.favoriteAction(with: area)
            self?.places[indexPath.item].toggleFavorite()
            self?.placesTableview.reloadRows(at: [indexPath], with: .automatic)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        HomeValues.HEIGHT_CELL
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let placeSelected = places[indexPath.row].place
        navigator.navigate(to: .details(place: placeSelected))
    }
}

// MARK: - MakeViewController
extension HomeViewController {
    static func makeViewController(navigator: HomeNavigator, viewModel: HomeViewModel) -> HomeViewController {
        guard let viewController = R.storyboard.home.homeViewController() else {
            preconditionFailure()
        }
        
        viewController.navigator = navigator
        viewController.viewModel = viewModel

        return viewController
    }
}
