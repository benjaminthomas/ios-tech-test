//
//  HomeNavigator.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit

class HomeNavigator: Navigator {
    enum Destination {
        case details(place: Place)
    }
    
    // MARK: Properties
    private weak var viewControllerProvider: ViewControllerProvider?
    private var router: Router
    
    init(viewControllerProvider: ViewControllerProvider, router: Router) {
        self.viewControllerProvider = viewControllerProvider
        self.router = router
    }
    
    func navigate(to destination: HomeNavigator.Destination) {
        let controller = makeViewController(to: destination)
        switch destination {
        case .details:
            router.push(controller)
        }
    }
}

private extension HomeNavigator {
    func makeViewController(to destination: HomeNavigator.Destination) -> Presentable {
        guard let viewControllerProvider = viewControllerProvider else { return UIViewController() }
        switch destination {
        case .details(let placeSelected):
            return viewControllerProvider.departuresViewController(router: router, place: placeSelected)
        }
    }
}

