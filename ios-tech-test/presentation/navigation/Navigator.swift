//
//  Navigator.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

protocol Navigator {
    associatedtype Destination
    
    func navigate(to destination: Destination)
}
