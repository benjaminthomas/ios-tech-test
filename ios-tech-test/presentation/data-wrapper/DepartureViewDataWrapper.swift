//
//  DepartureViewDataWrapper.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 17/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit

class DepartureViewDataWrapper {
    private let departure: Departure
    
    var image: UIImage? {
        switch departure.transport {
        case .bus:
            return R.image.bus()
        case .tramway:
            return R.image.tram()
        case .metro:
            return R.image.metro()
        default:
            return UIImage()
        }
    }
    
    var color: UIColor {
        UIColor(hexString: departure.color)
    }
    
    var name: String {
        departure.name
    }
    
    var code: String {
        departure.label
    }
    
    var departureDate: String {
        let calendar = Calendar.current
        let time = calendar.dateComponents([.hour, .minute], from: departure.departureAt)
        return "\(time.hour ?? 0):\(time.minute ?? 0)"
    }
    
    var departureWidth: CGFloat {
        code.count >= 3 ? 40.0 : 32.0
    }
    
    
    init(_ departure: Departure) {
        self.departure = departure
    }
}
