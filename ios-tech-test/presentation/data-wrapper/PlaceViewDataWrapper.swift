//
//  PlaceViewDataWrapper.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import UIKit

class PlaceViewDataWrapper {
    var place: Place
    
    var isFavorite: Bool {
        place.isFavorite
    }
    
    var name: String? {
        place.name
    }
    
    var aera: String {
        place.id
    }
    
    init(_ place: Place) {
        self.place = place
    }
    
    func toggleFavorite() {
        place.isFavorite.toggle()
    }
}
