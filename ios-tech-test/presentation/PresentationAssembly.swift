//
//  PresentationAssembly.swift
//  ios-tech-test
//
//  Created by Benjamin THOMAS on 15/08/2020.
//  Copyright © 2020 Benjamin THOMAS. All rights reserved.
//

import Swinject

class PresentationAssembly: Assembly {
    func assemble(container: Container) {
        // Can be used for inject components like loader or message
    }
}
